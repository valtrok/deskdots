# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Theme
ZSH_THEME="powerlevel10k/powerlevel10k"

# Fonts
source /home/corentin/.local/share/fonts/*.sh

# Plugins
plugins=(
    git
    dotbare
    zsh-autosuggestions
    zsh-syntax-highlighting
)

# Oh-my-zsh
export ZSH=/home/corentin/.oh-my-zsh
source $ZSH/oh-my-zsh.sh

# SSH
export SSH_KEY_PATH="~/.ssh/rsa_id"

# Fzf
source /usr/share/fzf/key-bindings.zsh

export FZF_CTRL_T_OPTS="--preview 'bat --style=numbers --color=always {} | head -500'"

# Aliases
source ~/.aliases

# Functions
source ~/.functions

# Wal
(cat ~/.cache/wal/sequences &)

# P10k
fpath=($fpath "/home/corentin/.zfunctions")

[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
