#!/bin/bash

wal -R

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null
do
    sleep 0.1;
done

# Launch top and bottom bars
polybar bottom &
polybar top &
if xrandr | grep "HDMI1 connected primary"; then
    polybar external &
fi
