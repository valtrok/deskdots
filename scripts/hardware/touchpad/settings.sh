#!/bin/bash

device_id=`xinput list | sed -n 's/.*DLL.*id=\([0-9][0-9]*\).*/\1/p'`

if [ -n "$device_id"]
then device_id=`xinput list | sed -n 's/.*Touchpad.*id=\([0-9][0-9]*\).*/\1/p'`
fi


tapping_prop=`xinput list-props $device_id | sed -n 's/.*Tapping Enabled (\([0-9][0-9]*\).*/\1/p'`
natural_sroll_prop=`xinput list-props $device_id | sed -n 's/.*Natural Scrolling Enabled (\([0-9][0-9]*\).*/\1/p'`

echo 'Touchpad ID : ' $device_id
echo 'Tapping Enabled prop ID : ' $tapping_prop
echo 'Natural Scrolling Enabled prop ID : ' $natural_sroll_prop

xinput set-prop $device_id $tapping_prop 1
xinput set-prop $device_id $natural_sroll_prop 1
