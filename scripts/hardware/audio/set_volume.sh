#!/bin/bash

SINK=`pacmd list-sinks | grep '* index' | cut -f5 -d' '`

echo pactl set-sink-$1 $SINK $2

pactl set-sink-$1 $SINK $2
