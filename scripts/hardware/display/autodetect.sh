#!/bin/bash
intern=eDP1
extern=HDMI1

if xrandr | grep "$extern disconnected"; then
    /home/corentin/.screenlayout/mono.sh
else
    /home/corentin/.screenlayout/dual.sh
fi
