#!/bin/bash
intern=eDP1
extern=HDMI1

if xrandr | grep "$intern connected primary "; then
    /home/corentin/.screenlayout/mirror.sh
elif xrandr | grep "$extern connected primary " | grep "+0+0"; then
    /home/corentin/.screenlayout/dual.sh
else
    /home/corentin/.screenlayout/mono.sh
fi
i3-msg restart
