#!/bin/sh

# Available Styles
#
# >> Edit these files and uncomment the desired colors/style.
#
# style_icons_blur    style_icons_full    style_icons_rainbow    style_icons_rainbow_sidebar
# style_icons_popup    style_normal    style_normal_grid    style_normal_grid_full    style_normal_grid_full_round
# style_normal_grid_round    style_normal_purple    style_normal_purple_alt    style_normal_rainbow
# style_normal_rainbow_sidebar

style="style_normal_grid"

rofi -no-lazy-grab -show run -theme ~/.config/rofi/launchers/"$style".rasi -run-list-command ". ~/scripts/rofi/zsh_aliases.sh" -run-command "/bin/zsh -i -c '{cmd}'"
