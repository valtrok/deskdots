# Base options
set $mod Mod4
font pango:OpenSans 8
floating_modifier $mod

# borders
for_window [class="^.*"] border pixel 1

# gaps
gaps inner 20
gaps outer 0

set $warpmouse exec xdotool mousemove --window $(xdotool getwindowfocus) $(($(xdotool getactivewindow getwindowgeometry --shell | grep WIDTH= | cut -c 7-)/2)) $(($(xdotool getactivewindow getwindowgeometry --shell | grep HEIGHT= | cut -c 8-)/2))

############################# Bindings #############################
# start a terminal
bindsym $mod+Return exec termite

# kill focused window
bindsym $mod+Shift+A kill

# start rofi
bindsym $mod+d exec ~/scripts/rofi/launcher.sh 

# change focus
bindsym $mod+j exec ~/scripts/i3/navigate left
bindsym $mod+k exec ~/scripts/i3/navigate down
bindsym $mod+i exec ~/scripts/i3/navigate up
bindsym $mod+l exec ~/scripts/i3/navigate right

# alternatively, you can use the cursor keys:
bindsym $mod+Left exec ~/scripts/i3/navigate left
bindsym $mod+Down exec ~/scripts/i3/navigate down
bindsym $mod+Up exec ~/scripts/i3/navigate up
bindsym $mod+Right exec ~/scripts/i3/navigate right

# move focused window
bindsym $mod+Shift+j move left; $warpmouse
bindsym $mod+Shift+k move down; $warpmouse
bindsym $mod+Shift+I move up; $warpmouse
bindsym $mod+Shift+l move right; $warpmouse

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left; $warpmouse
bindsym $mod+Shift+Down move down; $warpmouse
bindsym $mod+Shift+Up move up; $warpmouse
bindsym $mod+Shift+Right move right; $warpmouse

# split in horizontal orientation
bindsym $mod+h split h
# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+q focus parent

# marks
bindsym $mod+m exec i3-input -F 'mark %s' -l 1 -P 'Mark: '
bindsym $mod+g exec i3-input -F '[con_mark="%s"] focus' -l 1 -P 'Goto: '

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# Multi-monitor setup
workspace $ws1 output HDMI1 eDP1
workspace $ws2 output HDMI1 eDP1
workspace $ws3 output HDMI1 eDP1
workspace $ws4 output eDP1 HDMI1
workspace $ws5 output HDMI1 eDP1
workspace $ws6 output HDMI1 eDP1
workspace $ws7 output eDP1 HDMI1
workspace $ws8 output eDP1 HDMI1
workspace $ws9 output HDMI1 eDP1
workspace $ws10 output HDMI1 eDP1

# switch to workspace
bindsym $mod+ampersand workspace $ws1; $warpmouse
bindsym $mod+eacute workspace $ws2; $warpmouse
bindsym $mod+quotedbl workspace $ws3; $warpmouse
bindsym $mod+apostrophe workspace $ws4; $warpmouse
bindsym $mod+parenleft workspace $ws5; $warpmouse
bindsym $mod+minus workspace $ws6; $warpmouse
bindsym $mod+egrave workspace $ws7; $warpmouse
bindsym $mod+underscore workspace $ws8; $warpmouse
bindsym $mod+ccedilla workspace $ws9; $warpmouse
bindsym $mod+agrave workspace $ws10; $warpmouse

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1; $warpmouse
bindsym $mod+Shift+eacute move container to workspace $ws2; $warpmouse
bindsym $mod+Shift+3 move container to workspace $ws3; $warpmouse
bindsym $mod+Shift+4 move container to workspace $ws4; $warpmouse
bindsym $mod+Shift+5 move container to workspace $ws5; $warpmouse
bindsym $mod+Shift+6 move container to workspace $ws6; $warpmouse
bindsym $mod+Shift+egrave move container to workspace $ws7; $warpmouse
bindsym $mod+Shift+8 move container to workspace $ws8; $warpmouse
bindsym $mod+Shift+ccedilla move container to workspace $ws9; $warpmouse
bindsym $mod+Shift+agrave move container to workspace $ws10; $warpmouse

# reload the configuration file
bindsym $mod+Shift+c reload

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j exec ~/scripts/i3/resize shrink width 5 px or 5 ppt
        bindsym k exec ~/scripts/i3/resize grow height 5 px or 5 ppt
        bindsym i exec ~/scripts/i3/resize shrink height 5 px or 5 ppt
        bindsym l exec ~/scripts/i3/resize grow width 5 px or 5 ppt

        # same bindings, but for the arrow keys
        bindsym Left exec ~/scripts/i3/resize shrink width 5 px or 5 ppt
        bindsym Down exec ~/scripts/i3/resize grow height 5 px or 5 ppt
        bindsym Up exec ~/scripts/i3/resize shrink height 5 px or 5 ppt
        bindsym Right exec ~/scripts/i3/resize grow width 5 px or 5 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

# Make big
bindsym $mod+b resize set 1500 800

# Lock on $mod+shift+x
bindsym $mod+shift+x exec i3lock-fancy -t Locked

# toggle screen
bindsym $mod+p exec ~/scripts/hardware/display/cycle.sh

# Pulse Audio controls
bindsym XF86AudioRaiseVolume exec ~/scripts/hardware/audio/set_volume.sh volume +5% #increase sound volume
bindsym XF86AudioLowerVolume exec ~/scripts/hardware/audio/set_volume.sh volume -5% #decrease sound volume
bindsym XF86AudioMute exec ~/scripts/hardware/audio/set_volume.sh mute toggle # mute sound

# Sreen brightness controls
bindsym XF86MonBrightnessUp exec xbacklight +5 # increase screen brightness
bindsym XF86MonBrightnessDown exec xbacklight -5 # decrease screen brightness

# Media player controls
bindsym XF86AudioPlay exec playerctl play-pause
bindsym XF86AudioNext exec playerctl next
bindsym XF86AudioPrev exec playerctl previous

# Poweroff shortcut
bindsym $mod+Shift+F4 exec systemctl poweroff

############################# Other settings #############################

# Assign programs to workspaces
assign [class="Thunderbird"] $ws9
assign [class="KeePassXC"] $ws10

# Window sizes
for_window [class="Brave-browser"] resize set 2500 1150
for_window [workspace=$ws5] floating enable
for_window [workspace=$ws4] split v

############################# Startup #############################

# wall
exec_always --no-startup-id ~/scripts/wall/set-wall.sh &

# compositor
exec --no-startup-id picom -b --config ~/.picom.conf &

# touchpad settings
exec --no-startup-id ~/scripts/hardware/touchpad/settings.sh &

# nm-applet
exec --no-startup-id nm-applet &

# Auto lock
exec --no-startup-id xautolock -time 5 -locker "i3lock-fancy -t Locked" -detectsleep &

# Startup programs
#exec --no-startup-id ~/scripts/i3/startup.sh
exec --no-startup-id unclutter &
exec --no-startup-id dunst &
exec --no-startup-id pulseeffects --gapplication-service &
exec --no-startup-id autotiling &
exec --no-startup-id thunderbird &
exec --no-startup-id keepassxc &
